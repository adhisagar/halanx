package com.example.sagar.halanxapp.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.sagar.halanxapp.R;
import com.example.sagar.halanxapp.adapter.RoomAdapter;
import com.example.sagar.halanxapp.manager.PreferenceManager;
import com.example.sagar.halanxapp.model.RoomModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements RoomAdapter.RoomAdapterListener{

    private SearchView searchView;
    private RecyclerView recyclerView;
    private List<RoomModel> roomModelList=new ArrayList<>();
    RoomAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();
        jsonUse();
    }


    private void initialize() {
        recyclerView = findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RoomAdapter(MainActivity.this, roomModelList,this);
        recyclerView.setAdapter(adapter);
    }

    public void jsonUse() {

        String url = "http://testapi.halanx.com/homes/houses/?furnish_type=full,semi&house_type=independent,villa,apartment&accomodation_allowed=girls,boys,family&accomodation_type=flat,shared,private&rent_min=1000&rent_max=20000&latitude=28.6554&longitude=77.1646&radius=5";
        JsonObjectRequest objectRequest =new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    JSONArray result=response.getJSONArray("results");
                    for (int i=0;i<response.length();i++){

                        JSONObject resultShow=result.getJSONObject(i);
                        String name=resultShow.getString("name");

                        JSONObject address=resultShow.getJSONObject("address");

                        String street_address=address.getString("street_address");
                        String city=address.getString("city");
                        String state=address.getString("state");
                        String pincode=address.getString("pincode");
                        String country=address.getString("country");
                        String complete_address=address.getString("complete_address");
                        int latitude=address.getInt("latitude");
                        int longitude=address.getInt("longitude");

                        String description=resultShow.getString("description");
                        String furnish_type=resultShow.getString("furnish_type");
                        String house_size=resultShow.getString("house_size");
                        int rent_from=resultShow.getInt("rent_from");
                        int security_deposit_from=resultShow.getInt("security_deposit_from");
                        int available_flat_count=resultShow.getInt("available_flat_count");
                        int available_room_count=resultShow.getInt("available_room_count");
                        int available_bed_count=resultShow.getInt("available_bed_count");
                        String accomodation_allowed_str=resultShow.getString("accomodation_allowed_str");
                        String house_type=resultShow.getString("house_type");
                        String available_from=resultShow.getString("available_from");

                        boolean favorited=resultShow.getBoolean("favorited");
                        String cover_pic=resultShow.getString("cover_pic_url");
                        roomModelList.add(new RoomModel(name,description,furnish_type,house_size,cover_pic,rent_from,street_address,city,state,pincode,country,complete_address,latitude,longitude,
                                security_deposit_from,available_flat_count,available_room_count,available_bed_count,accomodation_allowed_str,house_type,available_from,favorited));
                        adapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        RequestQueue queue=Volley.newRequestQueue(this);
        queue.add(objectRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.option_menu,menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setQueryHint("Search by Name");

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                adapter.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }

    @Override
    public void onRoomSelected(RoomModel room) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.logout){
            new PreferenceManager(MainActivity.this).logout();
            startActivity(new Intent(MainActivity.this,LoginActivity.class));
            finishAffinity();
        }

        return super.onOptionsItemSelected(item);
    }
}
