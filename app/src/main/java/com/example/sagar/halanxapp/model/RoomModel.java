package com.example.sagar.halanxapp.model;

public class RoomModel {

    private int idNo;
    private String name;
    private String description;
    private String furnish_type;
    private String house_size;
    private String cover_pic;
    private int rent_from;
    private String street_address;
    private String city;
    private String state;
    private String pincode;
    private String country;
    private String complete_address;
    private int latitude;
    private int longitude;
    private int security_deposit_from;
    private int available_flat_count;
    private int available_room_count;
    private int available_bed_count;
    private String accomodation_allowed_str;
    private String house_type;
    private String available_from;
    private boolean favorited;

    public RoomModel(String name, String description, String furnish_type, String house_size, String cover_pic, int rent_from, String street_address, String city, String state, String pincode, String country, String complete_address, int latitude, int longitude, int security_deposit_from, int available_flat_count, int available_room_count, int available_bed_count, String accomodation_allowed_str, String house_type, String available_from, boolean favorited) {
        this.name = name;
        this.description = description;
        this.furnish_type = furnish_type;
        this.house_size = house_size;
        this.cover_pic = cover_pic;
        this.rent_from = rent_from;
        this.street_address = street_address;
        this.city = city;
        this.state = state;
        this.pincode = pincode;
        this.country = country;
        this.complete_address = complete_address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.security_deposit_from = security_deposit_from;
        this.available_flat_count = available_flat_count;
        this.available_room_count = available_room_count;
        this.available_bed_count = available_bed_count;
        this.accomodation_allowed_str = accomodation_allowed_str;
        this.house_type = house_type;
        this.available_from = available_from;
        this.favorited = favorited;
    }

    public int getIdNo() {
        return idNo;
    }

    public void setIdNo(int idNo) {
        this.idNo = idNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFurnish_type() {
        return furnish_type;
    }

    public void setFurnish_type(String furnish_type) {
        this.furnish_type = furnish_type;
    }

    public String getHouse_size() {
        return house_size;
    }

    public void setHouse_size(String house_size) {
        this.house_size = house_size;
    }

    public String getCover_pic() {
        return cover_pic;
    }

    public void setCover_pic(String cover_pic) {
        this.cover_pic = cover_pic;
    }

    public int getRent_from() {
        return rent_from;
    }

    public void setRent_from(int rent_from) {
        this.rent_from = rent_from;
    }

    public String getStreet_address() {
        return street_address;
    }

    public void setStreet_address(String street_address) {
        this.street_address = street_address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getComplete_address() {
        return complete_address;
    }

    public void setComplete_address(String complete_address) {
        this.complete_address = complete_address;
    }

    public int getLatitude() {
        return latitude;
    }

    public void setLatitude(int latitude) {
        this.latitude = latitude;
    }

    public int getLongitude() {
        return longitude;
    }

    public void setLongitude(int longitude) {
        this.longitude = longitude;
    }

    public int getSecurity_deposit_from() {
        return security_deposit_from;
    }

    public void setSecurity_deposit_from(int security_deposit_from) {
        this.security_deposit_from = security_deposit_from;
    }

    public int getAvailable_flat_count() {
        return available_flat_count;
    }

    public void setAvailable_flat_count(int available_flat_count) {
        this.available_flat_count = available_flat_count;
    }

    public int getAvailable_room_count() {
        return available_room_count;
    }

    public void setAvailable_room_count(int available_room_count) {
        this.available_room_count = available_room_count;
    }

    public int getAvailable_bed_count() {
        return available_bed_count;
    }

    public void setAvailable_bed_count(int available_bed_count) {
        this.available_bed_count = available_bed_count;
    }

    public String getAccomodation_allowed_str() {
        return accomodation_allowed_str;
    }

    public void setAccomodation_allowed_str(String accomodation_allowed_str) {
        this.accomodation_allowed_str = accomodation_allowed_str;
    }

    public String getHouse_type() {
        return house_type;
    }

    public void setHouse_type(String house_type) {
        this.house_type = house_type;
    }

    public String getAvailable_from() {
        return available_from;
    }

    public void setAvailable_from(String available_from) {
        this.available_from = available_from;
    }

    public boolean isFavorited() {
        return favorited;
    }

    public void setFavorited(boolean favorited) {
        this.favorited = favorited;
    }
}
