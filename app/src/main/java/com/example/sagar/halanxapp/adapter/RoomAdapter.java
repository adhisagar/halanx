package com.example.sagar.halanxapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.sagar.halanxapp.R;
import com.example.sagar.halanxapp.model.RoomModel;

import java.util.ArrayList;
import java.util.List;

public class RoomAdapter extends RecyclerView.Adapter<RoomAdapter.Viewholder> {
    Context context;
    private List<RoomModel> roomModelList;
    private List<RoomModel> roomListFiltered;
    private RoomAdapterListener listener;

    public RoomAdapter(Context context, List<RoomModel> roomModelList, RoomAdapterListener listener) {
        this.context = context;
        this.roomModelList = roomModelList;
        this.roomListFiltered = roomModelList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_layout, viewGroup, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder viewholder, int position) {

        viewholder.idNo.setText(String.valueOf(position + 1));
        viewholder.name.setText("Name :-" + roomListFiltered.get(position).getName());
        viewholder.description.setText("Description :-" + roomListFiltered.get(position).getDescription());
        viewholder.furnish_type.setText("Furnish_type :-" + roomListFiltered.get(position).getFurnish_type());
        viewholder.house_size.setText("House_Size :-" + roomListFiltered.get(position).getHouse_size());
        viewholder.rent_from.setText("Rent_from :-" + String.valueOf(roomListFiltered.get(position).getRent_from()));
        viewholder.street_address.setText("Street_Address :-" + roomListFiltered.get(position).getStreet_address());
        viewholder.city.setText("City :-" + roomListFiltered.get(position).getCity());
        viewholder.state.setText("State :-" + roomListFiltered.get(position).getState());
        viewholder.pincode.setText("Pincode :-" + roomListFiltered.get(position).getPincode());
        viewholder.country.setText("Country :-" + roomListFiltered.get(position).getCountry());
        viewholder.complete_address.setText("Complete_Address :-" + roomListFiltered.get(position).getComplete_address());
        viewholder.latitude.setText("Latitude :-" + String.valueOf(roomListFiltered.get(position).getLatitude()));
        viewholder.longitude.setText("Longitude :-" + String.valueOf(roomListFiltered.get(position).getLongitude()));
        viewholder.security_deposit_from.setText("Security_Deposit_From :-" + String.valueOf(roomListFiltered.get(position).getSecurity_deposit_from()));
        viewholder.available_flat_count.setText("Available_Flat_Count :-" + String.valueOf(roomListFiltered.get(position).getAvailable_flat_count()));
        viewholder.available_room_count.setText("Available_Room_Count :-" + String.valueOf(roomListFiltered.get(position).getAvailable_room_count()));
        viewholder.available_bed_count.setText("Available_Bed_Count :-" + String.valueOf(roomListFiltered.get(position).getAvailable_bed_count()));
        viewholder.accomodation_allowed_str.setText("Accomodation_Allowed_Str :-" + roomListFiltered.get(position).getAccomodation_allowed_str());
        viewholder.house_type.setText("House_Type :-" + roomListFiltered.get(position).getHouse_type());
        viewholder.available_from.setText("Available_from :-" + roomListFiltered.get(position).getAvailable_from());
        viewholder.favorited.setText("Favorited :-" + String.valueOf(roomListFiltered.get(position).isFavorited()));
        Glide.with(context).load(roomListFiltered.get(position).getCover_pic()).into(viewholder.cover_pic);
    }

    @Override
    public int getItemCount() {
        return roomListFiltered.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        private TextView idNo, name, description, furnish_type, house_size, rent_from, street_address, city, state, pincode, country, complete_address, latitude, longitude,
                security_deposit_from, available_flat_count, available_room_count, available_bed_count, accomodation_allowed_str, house_type, available_from, favorited;
        private ImageView cover_pic;

        public Viewholder(@NonNull View itemView) {
            super(itemView);
            idNo = itemView.findViewById(R.id.position);
            name = itemView.findViewById(R.id.name);
            description = itemView.findViewById(R.id.description);
            furnish_type = itemView.findViewById(R.id.furnish_type);
            house_size = itemView.findViewById(R.id.house_size);
            rent_from = itemView.findViewById(R.id.rent_from);
            street_address = itemView.findViewById(R.id.street_address);
            city = itemView.findViewById(R.id.city);
            state = itemView.findViewById(R.id.state);
            pincode = itemView.findViewById(R.id.pincode);
            country = itemView.findViewById(R.id.country);
            complete_address = itemView.findViewById(R.id.complete_address);
            latitude = itemView.findViewById(R.id.latitude);
            longitude = itemView.findViewById(R.id.longitude);
            security_deposit_from = itemView.findViewById(R.id.security_deposit_from);
            available_flat_count = itemView.findViewById(R.id.available_flat_count);
            available_room_count = itemView.findViewById(R.id.available_room_count);
            available_bed_count = itemView.findViewById(R.id.available_bed_count);
            accomodation_allowed_str = itemView.findViewById(R.id.accomodation_allowed_str);
            house_type = itemView.findViewById(R.id.house_type);
            available_from = itemView.findViewById(R.id.available_from);
            favorited = itemView.findViewById(R.id.favorited);
            cover_pic = itemView.findViewById(R.id.cover_pic);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onRoomSelected(roomListFiltered.get(getAdapterPosition()));
                }
            });
        }
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    roomListFiltered = roomModelList;
                } else {
                    List<RoomModel> filteredList = new ArrayList<>();
                    for (RoomModel row : roomModelList) {

                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    roomListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = roomListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                roomListFiltered = (ArrayList<RoomModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface RoomAdapterListener {
        void onRoomSelected(RoomModel room);
    }


}

